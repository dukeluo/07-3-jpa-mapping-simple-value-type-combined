package com.twuc.webApp.domain.composite;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SimpleMappingAndValueTest extends JpaTestBase {
    
    @Autowired
    private CompanyProfileRepository companyProfileRepository;
    
    @Autowired
    private UserProfileRepository userProfileRepository;

    @Test
    void should_get_and_save_a_company_profile() {
        ClosureValue<Long> expectedId = new ClosureValue<>();

        flushAndClear(em -> {
            final CompanyProfile profile =
                    companyProfileRepository.save(new CompanyProfile("Wuhan", "guanshan"));
            expectedId.setValue(profile.getId());
        });

        run(em -> {
            final CompanyProfile profile =
                    companyProfileRepository.findById(expectedId.getValue()).orElseThrow(
                            RuntimeException::new);

            assertEquals("Wuhan", profile.getCity());
            assertEquals("guanshan", profile.getStreet());
        });
    }

    @Test
    void should_get_and_save_a_user_profile() {
        ClosureValue<Long> expectedId = new ClosureValue<>();

        flushAndClear(em -> {
            final UserProfile profile =
                    userProfileRepository.save(new UserProfile(new Address("Wuhan", "guanshan")));
            expectedId.setValue(profile.getId());
        });

        run(em -> {
            final UserProfile profile =
                    userProfileRepository.findById(expectedId.getValue()).orElseThrow(
                            RuntimeException::new);

            assertEquals("Wuhan", profile.getAddress().getCity());
            assertEquals("guanshan", profile.getAddress().getStreet());
        });
    }
}
