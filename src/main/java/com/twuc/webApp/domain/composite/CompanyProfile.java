package com.twuc.webApp.domain.composite;

import javax.persistence.*;

@Entity(name = "company_profile")
public class CompanyProfile {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "city", length = 128, nullable = false)
    private String city;

    @Column(name = "street", length = 128, nullable = false)
    private String street;

    public CompanyProfile(String city, String street) {
        this.city = city;
        this.street = street;
    }

    public CompanyProfile() {
    }

    public Long getId() {
        return id;
    }

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }
}
