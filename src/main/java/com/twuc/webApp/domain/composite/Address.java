package com.twuc.webApp.domain.composite;

import javax.persistence.Column;
import javax.persistence.Embeddable;

//        | id              | bigint         | auto\_increment, primary key |
//        | address\_city   | varchar\(128\) | not null                     |
//        | address\_street | varchar\(128\) | not null                     |
@Embeddable
class Address {

    @Column(name = "_city", length = 128, nullable = false)
    private String city;

    @Column(name = "_street", length = 128, nullable = false)
    private String street;

    public Address(String city, String street) {
        this.city = city;
        this.street = street;
    }

    public Address() {
    }

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }
}
