package com.twuc.webApp.domain.composite;

import javax.persistence.*;

@Entity
public class UserProfile {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Embedded
    private Address address;

    public UserProfile(Address address) {
        this.address = address;
    }

    public UserProfile() {
    }

    public Address getAddress() {
        return address;
    }

    public Long getId() {
        return id;
    }
}
